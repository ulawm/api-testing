import requests


def test_if_json_is_not_empty():
    response = requests.get('https://pokeapi.co/api/v2/pokemon')
    body = response.json()
    assert len(body['results']>0)
    assert body['results']


def test_if_status_code_is_200():
    response = requests.get ('https://pokeapi.co/api/v2/pokemon')
    body = response.json()
    assert response.status_code == 200

def test_pokemon_is_1279():
    response = requests.get ('https://pokeapi.co/api/v2/pokemon')
    body = response.json()
    assert body["count"] == 1279


def test_response_time_under_1s():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    assert response.elapsed.total_seconds() < 1
    print(response.elapsed.total_seconds())

def test_size_of_response_is_under_100_kB():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    response_size_kb = len(response.content)/1000
    assert response_size_kb<100

def test_pagination():
    params = {
        "offset": 20,
        "limit": 10
    }
    response = requests.get('https://pokeapi.co/api/v2/pokemon', params=params)
    body = response.json()
    assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset']+1}/"
    assert body["results"][-1]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset']+ params['limit']}/"
    assert len(body["results"]) == params['limit']




